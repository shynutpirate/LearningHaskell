import Data.List
properDivisors x = [y |y <- [1..x-2], rem x y == 0]
numbers = concat[[x,z]| x <- [12..10000],let z = sum(properDivisors x),x == sum(properDivisors(z))]
main = do
  --print $ sum $ properDivisors $ sum $ properDivisors 284
  print $ nub $ numbers
  --print $ sum $ properDivisors 220
