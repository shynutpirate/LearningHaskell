collatz 1 = 1
collatz n = if odd n then 3 * n + 1 else n `div` 2
collatzSequence = terminate . iterate collatz
  where
    terminate (1:_) = [1]
    terminate (x:xs) = x:terminate xs

main = do
  --print $ length $ collatzSequence 13
  print $ collatzSequence 5
