toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod` 10]

digitPowers n = sum [x ^ 5 | x <- toDigits n]
main = do
    print $ [x | x <- [2..355000],digitPowers x == x]
    print $ sum [x | x <- [2..355000],digitPowers x == x]
