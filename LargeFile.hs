--toDigits = map digitToInt . show
main = do
  str <- readFile "number.txt"
  print $ lines str
  print $ [read x :: Int | x <- lines str]
