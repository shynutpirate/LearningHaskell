import Data.Char (digitToInt)
toDigits :: Int -> [Int]
toDigits = map digitToInt.show
--threeDigitedNumbers = [sum[x * 10 ^ 2,y * 10,z] | x <- [1..9], y <- [x+1..9],z <- [y+1..9]]
--checkLength x = length(x)
main = do
   print $ [sort(toDigits x) | x <- [12..99]]


  --print $ [[x,y,z] | x <- [1..9], y <- [x+1..9],z <- [y+1..9]]
