
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]


isPalindrome x = if (toDigits x) == (reverse (toDigits x)) then "yes" else "no"

main = do
  print $ isPalindrome 9009
