import qualified Data.Set as Set


pentagonals = [(n*(3*n - 1)) `div` 2 | n <- [1..3000]]

isPentagonal n = Set.member n (Set.fromList pentagonals)

sumDifferencePentagonals = [j - k | j <- pentagonals, k <- takeWhile (< j) pentagonals,
                      isPentagonal (j - k), isPentagonal (j + k)]

main = do
   print $ head sumDifferencePentagonals
