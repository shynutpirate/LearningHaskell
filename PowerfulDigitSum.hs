import Data.List
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]
powerGenerator = nub $ [(x^y)| x <- [2..99],y <- [10..99]]
maxDigitSum = maximum [sum(toDigits x)| x <- powerGenerator]
main = do
  print $ maxDigitSum
