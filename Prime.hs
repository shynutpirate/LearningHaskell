isFactor n f = rem n f == 0

isqrt = floor.sqrt.fromIntegral
isComposite n = or $ map(isFactor n)[2..isqrt n]
isPrime i = not $ isComposite i
primeGenerator = [ x | x <- [2..], isPrime x]
main = do
  print $ ((primeGenerator) !! 10000)
