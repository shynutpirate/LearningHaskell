triplet = head[[x,y,z] | x <- [2..1000],y <- [2..1000],z <- [2..1000],z^2 == x^2 + y^2, x+y+z == 1000]
main = do
  print $ product triplet
