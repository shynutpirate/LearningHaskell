
--toDigits = map digitToInt.show
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod` 10]
sumOfPowers = sum[ x ^ x | x <- [1..1000]]
main = do
  print $ reverse (take 10 (reverse (toDigits sumOfPowers)))
