import Data.List

score :: (Int, Int) -> Char -> (Int, Int)
score pts winner = if winner == 'X' then ((fst pts)+1, snd pts)
	else (fst pts, (snd pts) + 1)

cumScore :: (Int, Int) -> String -> (Int, Int)
cumScore pts winners = fold1 score pts winners

isGame :: (Int, Int) -> Bool
isGame pts = ((fst pts) + (snd pts) > 3 || (abs((fst pts)  - (snd pts))) > 1

win :: String -> Bool
win s =isGame $ cumScore(0, 0) s

noWin :: (String, String) -> Bool
noWin s = not $ isGame $ cumScore(0,0)(fst s)

SplitIt :: String -> (String, String)
SplitIt s = head $ dropWhile noWin $ [splitAt n s | n <- [4..length s]]

main = do
	let s1 = "XXY"
	let a1 = cumScore(0, 0)s1
	print s1
	print a1
	print $ win s1
