import Data.Char (digitToInt)
toDigits :: Int -> [Int]

toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]
factorial n = product (take n [1,2..])
main = do

  print $ sum $ [(factorial x) | x <- (toDigits 145)]
