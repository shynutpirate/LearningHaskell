toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]
toBinary 0 = []
toBinary x = toBinary (x `div` 2) ++ [x `mod` 2]
isPalindrome x = (toDigits x) == (reverse (toDigits x))
isPalindromeInBinary x = (toBinary x == (reverse (toBinary x))
main = do
  print $ sum(takeWhile(<1000000)[x | x <- [1..],(isPalindrome x) && (isPalindrome x)])
