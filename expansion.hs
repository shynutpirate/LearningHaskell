let exp x = [(x ** k) / (product [1..n]) | k <- [0..], n <- [1..k]]
main = do
  print $ sum (take 10 (exp 5))
