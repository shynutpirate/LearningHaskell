--import Data.Char (digitToInt)
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod` 10]
theproduct = [ x*y | x <- [100..999],y <- [100..999],(isPalindrome (x*y) )]
isPalindrome x = (toDigits x) == (reverse (toDigits x))

main = do
  print $ maximum (theproduct)
