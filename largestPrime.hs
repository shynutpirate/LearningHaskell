isFactor n f = rem n f == 0

isqrt = floor.sqrt.fromIntegral
isComposite n = or $ map(isFactor n)[2..isqrt n]
isPrime i = not $ isComposite i
primeFactors n = [x|x <- [2..isqrt n],rem n x == 0,isPrime x]
main = do
  print $ last(primeFactors 600851475143)
