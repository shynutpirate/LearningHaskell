import Data.List
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod` 10]
isNDigitedNumber = head ([x | x <- fib,(length (toDigits x) == 1000)])
fib = 1 : 1 : zipWith(+) fib(tail fib)
main = do

  print $ (elemIndex isNDigitedNumber fib)
