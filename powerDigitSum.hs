toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]

main = do
  print $ sum(toDigits (2 ^ 1000))
