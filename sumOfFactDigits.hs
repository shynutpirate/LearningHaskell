import Data.Char (digitToInt)
toDigits :: Int -> [Int]
--toDigits = map digitToInt . show
toDigits 0 = []
toDigits x = toDigits (x `div` 10) ++ [x `mod`10]
factorial n = product (take n [1,2..])
main = do
  --print $ factorial 100
  print $ sum $ toDigits $ factorial 100
