square x = x * x
sumOfSquares = sum [ square x | x <- [1,2..100]]
main = do
  print $ square (sum [1,2..100]) - sumOfSquares 
