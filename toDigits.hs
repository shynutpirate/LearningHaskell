import Data.Char (digitToInt)
toDigits :: Int -> [Int]
toDigits = map digitToInt . show

main = do
  print $ toDigits 50
